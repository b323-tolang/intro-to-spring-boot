package com.zuitt.wdc044.controllers;

import org.springframework.web.bind.annotation.*;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.*;

import java.util.List;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @GetMapping(value = "/posts")
    public ResponseEntity<Object> getAllPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.FOUND);
    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String token, @RequestBody Post post) {
        return postService.updatePost(postid, token, post);
    }

    @DeleteMapping(value = "/posts/{postid}")
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(postid, stringToken);
    }

    @GetMapping(value = "/myPosts")
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String token) {
        return postService.getUserPosts(token);
    }

}
