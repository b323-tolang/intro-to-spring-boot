package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// an interface contains behavior that a class implements
// an interface annotated with @Repository contains methods for database manipulation.
// by extending CrudRepository, PostRepository has inherited its pre-defined methods CRUD
public interface PostRepository extends CrudRepository<Post, Object> {

    List<Post> findByUserUsername(String authenticatedUser);
}
